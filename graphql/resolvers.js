const User = require('../models/User')
const UserController = require('../controllers/user')


module.exports = {
	Mutation: {
		register: (_, args) => {
			return UserController.register(args)
		}
	}
}