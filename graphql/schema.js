const {gql} = require('apollo-server-express')

module.exports = gql`


	type Query{

		emailExists: Boolean

	}

	type Mutation{

		register (

			firstName: String!,
			lastName: String!,
			email: String!,
			mobileNumber: String!,
			password: String!

		): Boolean


	}



`