const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({


	firstName: {

		type: String,
		required: [true, 'First Name is Required.']

	},
	lastName: {

		type: String,
		required: [true, 'Last Name is Required']

	},

	email: {

		type: String,
		required: [true, 'Email is required']

	},

	password: {

		type: String

	},

	loginType: {

		type: String,
		required: [true, 'Login type is required']

	},
	mobileNumber:{

		type: String

	},
	isAdmin: {

		type: Boolean,
		default: false

	},
	enrollments: [

		{

			courseId: {

				type: String,
				required: [true, 'Course ID is required']

			},
			enrolledOn: {

				type: Date,
				default: new Date()

			},
			status: {

				type: String,
				default: 'Enrolled' //Alt values are canncelled and completed

			}

		}


	]




})


module.exports = mongoose.model('user', userSchema)