//index js = initialize with index.js
//require express first 
const express = require('express')
const app = express()
const mongoose = require('mongoose')
const userRoutes = require('./routes/user')
const { ApolloServer } = require('apollo-server-express')

app.use(express.json())
//encodes server url to readable format
app.use(express.urlencoded({extended: true}))

//listens from our server to echo status
app.listen(process.env.PORT || 4000, ()=>{


//julius back tick/template literal ES6 instantiation
	console.log(`API is now online on port ${process.env.PORT || 4000}`)


})



mongoose.connection.once('open', ()=>{

	console.log('We are connected to local MongoDB server')

})

/*local to mongoDB:
	
	mongoose.connect('mongodb://localhost:27017/course_booking_demo', {
	
	useNewURLParse: true,
	useUnifiedTopology: true
	

	})

*/

//addadminforDB in atlas
mongoose.connect('mongodb+srv://teejae:1234teejae@b49ecommercetj-ggnuu.mongodb.net/men_g?retryWrites=true&w=majority', {

	useNewUrlParser: true,
	useUnifiedTopology: true


})

//add whitelist 00000 access anywehre
// /api used to distinguish front and backend

//connect route files
app.use('/api/users', userRoutes)

new ApolloServer({


	typeDefs: require('./graphql/schema'),
	resolvers: require('./graphql/resolvers')


}).applyMiddleware({app, path: '/graphql'})